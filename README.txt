
 ┌─┐┬ ┬┬─┐┌─┐┌┐┌┌┬┐┌─┐
 │  ├─┤├┬┘│ ││││├┼┤└─┐
 └─┘┴ ┴┴└─└─┘┘└┘└┴┘└─┘

 Software used to store a cache from a software/plugin/script
 execution for a specified time.

 If execution time exceeded defined 'timeout', Chronos will
 terminate plugin execution.

 Stored cache data will be allocated on current execution directory.
 ──────────────────────────────────────────────────────────────────────────────
 Examples:
 ------------------------------------------------------------------------------
    C:\>SomeApp\Chronos.exe [params]
       ^
       └─── cache will be created in C:\Cache
 ------------------------------------------------------------------------------
    C:\SomeApp>Chronos.exe [params]
              ^
              └─── cache will be created in C:\SomeApp\Cache
 ──────────────────────────────────────────────────────────────────────────────
 Params Explanation:
 ------------------------------------------------------------------------------
    Chronos.exe 30 10 C:\Windows\System32\cmd.exe /c dir
                ^  ^  ^                           ^    ^
                │  |  └ plugin             plugin └──┬─┘
                │  └─── cache time         params ───┘
                └────── timeout
 ------------------------------------------------------------------------------
    Chronos.exe clearCache
                ^        ^
                └────┬───┘
                     └─── clear all Chronos cache
 ──────────────────────────────────────────────────────────────────────────────
 Use Examples:
 ------------------------------------------------------------------------------
    Chronos.exe 300 60 C:\Scripts\InstallWebFeatures.ps1
    Chronos.exe 30 10 C:\monitoring\check_uptime.vbs 50 60
    Chronos.exe clearCache

